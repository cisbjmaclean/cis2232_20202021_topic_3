package info.hccis.camper.controllers;

import info.hccis.camper.bo.CamperTypeBO;
import info.hccis.camper.jpa.entity.CamperType;
import info.hccis.camper.repositories.CamperTypeRepository;
import info.hccis.camper.util.CisUtility;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BaseController {

    private final CamperTypeRepository camperTypeRepository;

    public BaseController(CamperTypeRepository ctr) {
        camperTypeRepository = ctr;
    }
    
    @RequestMapping("/")
    public String home(HttpSession session) {
        
        //BJM 20200602 Issue#1 Set the current date in the session
        String currentDate = CisUtility.getCurrentDate("yyyy-MM-dd");
        session.setAttribute("currentDate", currentDate); 
        
        //Load the camper types into the session
//        CamperTypeBO camperTypeBO = new CamperTypeBO();
//        ArrayList<CamperType> camperTypes = new ArrayList();
//        camperTypes = camperTypeBO.load();
        ArrayList<CamperType> camperTypes = (ArrayList<CamperType>) camperTypeRepository.findAll();
        session.setAttribute("camperTypes", camperTypes);
        System.out.println("BJM, loaded "+camperTypes.size()+" camper types");
        
        HashMap<Integer, String> camperTypesMap = CamperTypeBO.getCamperTypesMap();
        camperTypesMap.clear();
        for(CamperType current: camperTypes){
            camperTypesMap.put(current.getId(), current.getDescription());
        }
        
        
        
        return "index";
    }

    @RequestMapping("/about")
    public String about() {
        return "other/about";
    }
}
