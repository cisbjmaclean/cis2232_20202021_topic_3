package info.hccis.teetimebooker.bo;

import info.hccis.teetimebooker.dao.BookingDAO;
import info.hccis.teetimebooker.entity.jpa.Booking;
import java.util.ArrayList;

/**
 * Business logic associated with bookings
 *
 * @author bjm
 * @since 2020-05-28
 */
public class BookingBO {

    public static ArrayList<Booking> selectAll(){
        BookingDAO bookingDAO = new BookingDAO();
        return bookingDAO.selectAll();
    }
    
    public static void addBooking(Booking booking) {

        System.out.println("Booking object about to be added to the database"
                + ""
                + "\n" + booking.toString());

        //Add that booking to the database
        BookingDAO bookingDAO = new BookingDAO();

        if (booking.getId() == 0) {
            bookingDAO.insert(booking);
        } else {
            bookingDAO.update(booking);
        }

    }

}
